-- MySQL Script generated by MySQL Workbench
-- Thu Jan 28 16:19:34 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema menumeu
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema menumeu
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `menumeu` DEFAULT CHARACTER SET utf8 ;
USE `menumeu` ;

-- -----------------------------------------------------
-- Table `menumeu`.`requests`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `menumeu`.`requests` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `idProduct` VARCHAR(45) NULL,
  `client` VARCHAR(45) NULL,
  `date` VARCHAR(45) NULL,
  `date_update` VARCHAR(45) NULL,
  `editor` VARCHAR(45) NULL,
  `status` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `menumeu`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `menumeu`.`users` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `date` VARCHAR(45) NULL,
  `date_update` VARCHAR(45) NULL,
  `password` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `menumeu`.`foods`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `menumeu`.`foods` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `price` VARCHAR(45) NULL,
  `image` VARCHAR(45) NULL,
  `description` VARCHAR(255) NULL,
  `date` VARCHAR(45) NULL,
  `date_update` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
