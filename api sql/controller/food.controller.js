const Food = require("../model/food.model.js");
const moment = require("moment");
const global = require("../global/global.js");

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    // Create a user
    const food = {
        description: req.body.description,
        image: req.body.image,
        name: req.body.name,
        date: moment().format('DD/MM/YYYY, h:mm:ss a')
    };

    // Save User in the database
    const modelFunction = () => Food.create(food, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the food."
            });
        else res.send(data);
    });
    global.validateJwt(modelFunction, req, res);
};

exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    const modelFunction = () => Food.updateById(
        req.body.id,
        new Food(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found Food with id ${req.body.id}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating Food with id " + req.body.id
                    });
                }
            } else res.send(data);
        }
    );
    global.validateJwt(modelFunction, req, res);
};

exports.findAll = (req, res) => {
    const modelFunction = () => Food.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        else res.send(data);
    });
    global.validateJwt(modelFunction, req, res);
};

// Find a single Food with a FoodId
exports.findOne = (req, res) => {
    const modelFunction = () => Food.findByName(req.body.name, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Food with name ${req.body.name}.`
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving Food with name " + req.body.name
                });
            }
        } else res.send(data);
    });
    global.validateJwt(modelFunction, req, res);
};

exports.delete = (req, res) => {
    const modelFunction = () => Food.remove(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Food with id ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete Food with id " + req.params.id
                });
            }
        } else res.send({ message: `Food was deleted successfully!` });
    });
    global.validateJwt(modelFunction, req, res);
};
// Delete all Foods from the database.

exports.deleteAll = (req, res) => {
    const modelFunction = () => Food.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all Foods."
            });
        else res.send({ message: `All Foods were deleted successfully!` });
    });
    global.validateJwt(modelFunction, req, res);
};


