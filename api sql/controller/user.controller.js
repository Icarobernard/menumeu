const User = require("../model/user.model.js");
const CryptoJS = require("crypto-js");
const moment = require("moment");
const global = require("../global/global.js");

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    // Create a user
    const user = {
        email: req.body.email,
        name: req.body.name,
        password: CryptoJS.AES.encrypt(req.body.password, req.body.name).toString(),
        date: moment().format('DD/MM/YYYY, h:mm:ss a')
    };

    // Save User in the database
    const modelFunction = () => User.create(user, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the user."
            });
        else res.send(data);
    });
    global.validateJwt(modelFunction, req, res);
};

exports.login = (req, res) => {

    const user = new User({
        name: req.body.name,
        password: req.body.password
    });
    User.login(user, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found user ${user.name}.`,
                    status: 404
                });
            } else {
                res.status(500).send({
                    message: "Error retrieving user " + user.name
                });
            }
        } else res.send(data);
    });
};

exports.findOne = (req, res) => {

    const user = new User({
        name: req.body.name,
    });

    User.findByUsername(user.name, (err, data) => {
        if (err) {
            console.log(err)
            if (err.kind === "not_found") {
                res.status(404).send(
                    false
                );
            } else {
                res.status(500).send({
                    message: "Error retrieving User with name " + user.name
                });
            }
        } else res.send(data);
    });
};


exports.findAll = (req, res) => {
    const modelFunction = () => User.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        else res.send(data);
    });
    global.validateJwt(modelFunction, req, res);
}

exports.delete = (req, res) => {
    const modelFunction = () => User.remove(req.body.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Customer with id ${req.body.id}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete user with id " + req.body.id
                });
            }
        } else res.send({ message: `User was deleted successfully!` });
    });
    global.validateJwt(modelFunction, req, res);
};

exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    const modelFunction = () => User.updateById(
        req.body.id,
        new User(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found user with id ${req.body.id}.`
                    });
                } else {
                    res.status(500).send({
                        message: "Error updating user with id " + req.body.id
                    });
                }
            } else res.send(data);
        }
    );
    global.validateJwt(modelFunction, req, res);
};

