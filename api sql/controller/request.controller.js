const Model = require("../model/request.model.js");
const moment = require("moment");
const nameController = "request";
const global = require("../global/global.js");

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    // Create a user
    const object = {
        idProduct: req.body.idProduct,
        client: req.body.client,
        editor: req.body.editor,
        description: req.body.description,
        status: '1',
        date: moment().format('DD/MM/YYYY, h:mm:ss a')
    };

    // Save User in the database
    const modelFunction = () => Model.create(object, (err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || `Some error occurred while creating the ${nameController}.`
            });
        else res.send(data);
    });
    global.validateJwt(modelFunction, req, res);
};

exports.update = (req, res) => {
    // Validate Request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    const modelFunction = () => Model.updateById(
        req.body.id,
        new Model(req.body),
        (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.status(404).send({
                        message: `Not found ${nameController} with id ${req.body.id}.`
                    });
                } else {
                    res.status(500).send({
                        message: `Error updating ${nameController} with id ${req.body.id}`
                    });
                }
            } else res.send(data);
        }
    );
    global.validateJwt(modelFunction, req, res);
};

exports.findAll = (req, res) => {
    const modelFunction = () => Model.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || `Some error occurred while retrieving ${nameController}.`
            });
        else res.send(data);
    });
    global.validateJwt(modelFunction, req, res);
};

// Find a single ${nameController} with a ${nameController}Id
exports.findOne = (req, res) => {
    const modelFunction = () => Model.findById(req.body.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found ${nameController} with id ${req.body.id}.`
                });
            } else {
                res.status(500).send({
                    message: `Error retrieving ${nameController} with id ${req.body.id}`
                });
            }
        } else res.send(data);
    });
    global.validateJwt(modelFunction, req, res);
};

exports.delete = (req, res) => {
    const modelFunction = () => Model.remove(req.params.id, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found ${nameController} with id ${req.params.id}.`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete with id " + req.params.id
                });
            }
        } else res.send({ message: `${nameController} was deleted successfully!` });
    });
    global.validateJwt(modelFunction, req, res);
};
// Delete all Customers from the database.

exports.deleteAll = (req, res) => {
    const modelFunction = () => Model.removeAll((err, data) => {
        if (err)
            res.status(500).send({
                message:
                    err.message || `Some error occurred while removing all ${nameController}.`
            });
        else res.send({ message: `All ${nameController} were deleted successfully!` });
    });
    global.validateJwt(modelFunction, req, res);
};


