const User = require("../model/user.model.js");

 exports.validateJwt = (func, req, res) => {
  const unautorized = () => res.status(401).send({
      message: "Acess denied!"
  });
  if (!req.header('jwt')) {
      unautorized()
  } else {
      User.findByUsername(req.header('jwt'), (err, data) => {
          if (err) {
              console.log(err)
              if (err.kind === "not_found") {
                  unautorized()
              } else {
                  res.status(500).send({
                      message: "Error retrieving User with name " + user.name
                  });
              }
          } else {
              //res.send(data);   
              func();
          }
      });
  };
}