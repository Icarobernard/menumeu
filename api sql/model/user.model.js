const sql = require("../db");
const moment = require("moment");
const CryptoJS = require("crypto-js");
// constructor
const User = function (user) {
    this.email = user.email;
    this.name = user.name;
    this.password = user.password;
};

User.create = (newUser, result) => {
    
    sql.query("INSERT INTO users SET ?", newUser, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("created user: ", { id: res.insertId, ...newUser });
        result(null, { id: res.insertId, ...newUser });
    });
};

User.login = (user, result) => {
    const CryptoJS = require('crypto-js')
    var userHash;
    sql.query(`SELECT password FROM users WHERE name = "${user.name}"` , (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found hash: ", res[0].password);
            userHash =  res[0].password
            var decrypted = CryptoJS.AES.decrypt(userHash, user.name);
            console.log("value", decrypted.toString(CryptoJS.enc.Utf8))
            if(user.password == decrypted.toString(CryptoJS.enc.Utf8)){
                sql.query(`SELECT * FROM users WHERE name = "${user.name}"` , (err, res) => {
                    if (err) {
                        console.log("error: ", err);
                        result(err, null);
                        return;
                    }
            
                    if (res.length) {
                        console.log("found customer: ", res[0]);
                        result(null, res[0]);
                        return;
                    }
            
                    // not found Customer with the id
                    result({ kind: "not_found" }, null);
                });
            }else{
                result({ kind: "not_found" }, null);
            }
            
        }
        // not found Customer with the id
        // result({ kind: "not_found" }, null);
    });

};

User.findByUsername = (name, result) => {
    var value;
    sql.query(`SELECT name FROM users WHERE name = '${name}'`, (err, res) => {
        
        if (err) {
            console.log("error: ", err);
            value=false;
            result(err, value);
            return;
        }

        if (res.length) {
            console.log("found user: ", res[0]);
            value=true;
            result(null, value);
            return;
        }
        result({ kind: "not_found" }, null);
    });
};

User.getAll = result => {
    sql.query("SELECT * FROM users", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("users: ", res);
        result(null, res);
    });
};

User.updateById = (id,user, result) => {
    var now = moment().format('DD/MM/YYYY, h:mm:ss a')
    var password = CryptoJS.AES.encrypt(user.password, user.name).toString()
    sql.query(
        `UPDATE users SET email = ?, name = ?, password = ?, date_update = ? WHERE id = ?`,
        [user.email, user.name, password, now, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }
            console.log("success update: ", { user });
            result(null, { user });
        }
    );
};

User.remove = (id, result) => {
    sql.query("DELETE FROM users WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found Customer with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted customer with id: ", id);
        result(null, res);
    });
};

module.exports = User;