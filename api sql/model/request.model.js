const sql = require("../db");
const moment = require("moment");
// constructor
const Request = function (request) {
    this.idProduct = request.idProduct;
    this.client = request.client;
    this.editor = request.editor;
    this.date = request.date;

    this.status = request.status;
};

Request.create = (newRequest, result) => {
    sql.query("INSERT INTO requests SET ?", newRequest, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("Request created: ", { id: res.insertId, ...newRequest });
        result(null, { id: res.insertId, ...newRequest });
    });
};

Request.getAll = result => {
    sql.query(`SELECT r.id,r.client, r.editor, r.description,
     r.date_update, r.date, f.name, f.image, r.status FROM requests r 
     LEFT JOIN foods f ON f.id = r.idProduct ORDER BY r.status `, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("requests: ", res);
        result(null, res);
    });
};

Request.findById = (id, result) => {
    sql.query(`SELECT r.client, r.editor, r.description, 
    r.date_update, r.date, f.name, f.image, r.status FROM requests r 
    LEFT JOIN foods f ON f.id = r.idProduct WHERE r.id = ${id}`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found customer: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

Request.updateById = (id, request, result) => {
    var now = moment().format('DD/MM/YYYY, h:mm:ss a')
    sql.query(
        `UPDATE requests SET status = ?, editor = ?, date_update = ? WHERE id = ?`,
        [request.status, request.editor, now, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }
            console.log("updated request: ", { id: id, ...request });
            result(null, { id: id, ...request });
        }
    );
};

Request.remove = (id, result) => {
    sql.query("DELETE FROM requests WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        if (res.affectedRows == 0) {
            // not found FOOD with the id
            result({ kind: "not_found" }, null);
            return;
        }
        console.log("deleted request with id: ", id);
        result(null, res);
    });
};

Request.removeAll = result => {
    sql.query("DELETE FROM requests", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log(`deleted ${res.affectedRows} resquest`);
        result(null, res);
    });
};



module.exports = Request;