const sql = require("../db");
const moment = require("moment");
// constructor
const Food = function (food) {
    this.description = food.description;
    this.image =food.image;
    this.price = food.price;
    this.name = food.name;
    this.date = food.date;
};

Food.create = (newFood, result) => {
    sql.query("INSERT INTO foods SET ?", newFood, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("Food created: ", { id: res.insertId, ...newFood });
        result(null, { id: res.insertId, ...newFood });
    });
};

Food.getAll = result => {
    sql.query("SELECT * FROM foods", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log("foods: ", res);
        result(null, res);
    });
};

Food.findByName = (name, result) => {
    sql.query(`SELECT * FROM foods WHERE name = '${name}'`, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found food: ", res[0]);
            result(null, res[0]);
            return;
        }

        // not found Customer with the id
        result({ kind: "not_found" }, null);
    });
};

Food.updateById = (id,food, result) => {
    var now =  moment().format('DD/MM/YYYY, h:mm:ss a');
    sql.query(
        `UPDATE foods SET image = ?, name = ?, description = ?, price= ?, date_update = ? WHERE id = ?`,
        [food.image, food.name, food.description, food.price, now, id],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(null, err);
                return;
            }
            if (res.affectedRows == 0) {
                // not found Customer with the id
                result({ kind: "not_found" }, null);
                return;
            }
            console.log("updated food: ", { id: id, ...food });
            result(null, { id: id, ...food });
        }
    );
};

Food.remove = (id, result) => {
    sql.query("DELETE FROM foods WHERE id = ?", id, (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        if (res.affectedRows == 0) {
            // not found FOOD with the id
            result({ kind: "not_found" }, null);
            return;
        }
        console.log("deleted food with id: ", id);
        result(null, res);
    });
};

Food.removeAll = result => {
    sql.query("DELETE FROM foods", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        console.log(`deleted ${res.affectedRows} foods`);
        result(null, res);
    });
};



module.exports = Food;