var userValues = JSON.parse(sessionStorage.getItem('values'))
export const createAccount = (params) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(params),
  };
  fetch("/create/user", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result)
        alert(result.message)
      },
      (error) => {
        alert("erro")
      }
    )
}
export const getUser = (params) => {
  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'jwt': userValues == null ? null : `${userValues.name}`,
    },
    body: JSON.stringify(params),
  };
  fetch("/users", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        params(result)
        return result;
      },
      (error) => {
        console.log(error)
      }
    )
}

export const deleteUser = (params) => {
  const requestOptions = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'jwt': userValues == null ? null : `${userValues.name}`,
    },
    body: JSON.stringify(params),
  };
  fetch("/delete/user", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        params(result)
      },
      (error) => {
        console.log(error)
      }
    )
}

export const login = (params) => {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(params),
  };
  fetch("/login", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result)
        if (result.status) {
          alert(result.message)
        } else {
          alert("Login success")
          sessionStorage.setItem('name', result.name)
          sessionStorage.setItem('values', JSON.stringify(result));
          window.location.href = '/home'
        }
      },
      (error) => {
        alert(error)
      }
    )
}

export const validateName = (params, changeValue) => {
  console.log("validate", params)
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(params),
  };
  fetch("/validate", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result)
        changeValue(result)
        if (result.status) {
          alert(result.message)
        } else {
          return result
        }
      },
      (error) => {
        alert(error)
      }
    )
}

export const createRequest = (params, toggle) => {
  const requestOptions = {
    method: 'Post',
    headers: {
      'Content-Type': 'application/json',
      'jwt': userValues == null ? null : `${userValues.name}`
    },
    body: JSON.stringify(params),
  };
  fetch("/create/request", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        toggle()
        console.log(result)
      },
      (error) => {
        console.log(error)
      }
    )
}

export const updateRequest = (params) => {
  const requestOptions = {
    method: 'Post',
    headers: {
      'Content-Type': 'application/json',
      'jwt': userValues == null ? null : `${userValues.name}`
    },
    body: JSON.stringify(params),
  };
  fetch("/update/request", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result)
      },
      (error) => {
        console.log(error)
      }
    )
}

export const getAllRequests = (params) => {
  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'jwt': userValues == null ? null : `${userValues.name}`
    },
    body: JSON.stringify(params),
  };
  fetch("/requests", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        params(result)
        console.log(result)
      },
      (error) => {
        console.log(error)
      }
    )
}

export const getAllFoods = (params) => {
  const requestOptions = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'jwt': userValues == null ? null : `${userValues.name}`
    },
    body: JSON.stringify(params),
  };
  fetch("/foods", requestOptions)
    .then(res => res.json())
    .then(
      (result) => {
        console.log(result)
        params(result)
      },
      (error) => {
        console.log(error)
      }
    )
}
