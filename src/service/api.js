import Axios from 'axios'

const baseURL = 'http://localhost:1001/'

// export const baseIMG = 'http://www.teste-rbmweb.com.br/'
const api = Axios.create({
  baseURL: process.env.API ? process.env.API + '/' : baseURL,
  headers: {
    common: {
      jwt: localStorage.getItem('is_authenticate')
    }
  }
})


export { baseURL }

export default api