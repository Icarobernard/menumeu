/*!

=========================================================
* BLK Design System PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/blk-design-system-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {
  Button,
  UncontrolledCollapse,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  NavbarBrand,
  Navbar,
  NavItem,
  Nav,
  Container,
  Row,
  Col,
  UncontrolledTooltip,
  NavLink
} from "reactstrap";
import LoginModal from '../Modal/LoginModal'
class ColorNavbar extends React.Component {
  state = {
    navbarColor: "navbar-transparent",
    modal: false
  };
  componentDidMount() {
    window.addEventListener("scroll", this.changeNavbarColor);
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.changeNavbarColor);
  }
  changeNavbarColor = () => {
    if (
      document.documentElement.scrollTop > 299 ||
      document.body.scrollTop > 299
    ) {
      this.setState({
        navbarColor: "bg-default"
      });
    } else if (
      document.documentElement.scrollTop < 300 ||
      document.body.scrollTop < 300
    ) {
      this.setState({
        navbarColor: "navbar-transparent"
      });
    }
  };

  toggle = () => {
    this.setState({ modal: !this.state.modal })
  }
  render() {
    return (
      <>
        <Navbar className={"fixed-top " + this.state.navbarColor} expand="lg">
          <Container>
            <div className="navbar-translate">
              <NavbarBrand to="/home" tag={Link} id="tooltip6619950104">
                <span>Menumeu•</span>
              </NavbarBrand>
              <UncontrolledTooltip delay={0} target="tooltip6619950104">
                Designed and Coded by Creative ICO
              </UncontrolledTooltip>
              <button className="navbar-toggler" id="navigation">
                <span className="navbar-toggler-bar bar1" />
                <span className="navbar-toggler-bar bar2" />
                <span className="navbar-toggler-bar bar3" />
              </button>
            </div>

            <UncontrolledCollapse navbar toggler="#navigation">
              <div className="navbar-collapse-header">
                <Row>
                  <Col className="collapse-brand" xs="6">
                    <a href="#pablo" onClick={e => e.preventDefault()}>
                      Menumeu•
                    </a>
                  </Col>
                  <Col className="collapse-close text-right" xs="6">
                    <button className="navbar-toggler" id="navigation">
                      <i className="tim-icons icon-simple-remove" />
                    </button>
                  </Col>
                </Row>
              </div>
              <Nav className="ml-auto" navbar>
                {sessionStorage.getItem('values') &&
                  <>
                    {/* <NavItem className="active">
                      <NavLink
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        <i className="tim-icons icon-bullet-list-67" />
                         Gerenciar Produtos
                      </NavLink>
                    </NavItem> */}
                    <NavItem className="active">
                      <NavLink
                        to="/user" tag={Link}
                      >
                        <i className="tim-icons icon-single-02" />
                          Gerenciar usuários
                      </NavLink>
                    </NavItem>
                    <NavItem>
                      <NavLink
                        to="/request" tag={Link}
                        onClick={e => window.href="/request"}
                      >
                        <i className="tim-icons icon-paper" />
                          Pedidos
                  </NavLink>
                    </NavItem>
                    <NavItem>
                      <Button
                        className="nav-link"
                        color="secondary"
                        onClick={() => { sessionStorage.removeItem('values'); window.location.reload() }}
                        //href="https://www.creative-tim.com/product/blk-design-system-pro-react?reaf=blkdspr-pages-navbar"
                        size="sm"
                        target="_blank"
                      >
                        <p>Logout</p>
                      </Button>
                    </NavItem>
                  </>
                }
                {!sessionStorage.getItem('values') &&
                  <NavItem>
                    <Button
                      className="nav-link"
                      color="info"
                      onClick={() => { this.toggle() }}
                      //href="https://www.creative-tim.com/product/blk-design-system-pro-react?reaf=blkdspr-pages-navbar"
                      size="sm"
                      target="_blank"
                    >
                      <p>Login</p>
                    </Button>
                  </NavItem>
                }
              </Nav>

            </UncontrolledCollapse>
          </Container>
        </Navbar>
        <LoginModal modal={this.state.modal} toggle={this.toggle}></LoginModal>
      </>
    );
  }
}

export default ColorNavbar;
