import React, { useState, useEffect } from 'react'
import {
  Button,
  Badge,
  Table,
  UncontrolledTooltip,

} from "reactstrap";
import { getAllRequests, updateRequest } from "../../store/action"
export default function TableRequest() {
  const [requests, setRequest] = useState([]);
  useEffect(() => {
    getAllRequests(setRequest)
  },[])

  return (
    <div>
      {requests.length == 0 ?
        <h3>Sem pedidos</h3>
        : <Table
          className="tablesorter table-shopping"
          responsive
        >
          <thead>
            <tr>
              <th className="text-center" />
              <th>Pedido</th>
              <th>Cliente</th>
              <th>Horário</th>
              <th className="text-right">Operação</th>
              {/* <th className="text-right">Qty</th>
          <th className="text-right">Amount</th> */}
            </tr>
          </thead>
          {
            requests && requests.map((request) =>
              <tbody>
                <tr>
                  <td>
                    <div className="img-container">
                      <img
                        alt="..."
                        src={request.image}
                      />
                    </div>
                  </td>
                  <td className="td-name">
                    <a
                      href="#pablo"
                      onClick={e => e.preventDefault()}
                    >
                      {request.client}
                    </a>
                    <br />
                    <small>
                      {request.name}
                    </small>
                  </td>
                  <td>{request.date}</td>
                  <td>
                    {request.status == 1 && <Badge color="info">Aguardando</Badge>}
                    {request.status == 2 && <Badge color="success">Atendido</Badge>}
                    {request.status == 3 && <Badge color="warning">Cancelado</Badge>}
                  </td>

                  <td className="td-actions">
                    {request.status == 1 ?
                      <Button
                        className="btn-link"
                        color="success"
                        id="tooltip653500052"
                        type="button"
                      >
                        <i className="tim-icons icon-check-2" />
                      </Button>
                      : request.date_update
                    }
                    <UncontrolledTooltip
                      delay={0}
                      placement="left"
                      target="tooltip653500052"
                    >
                      Atender pedido
                   </UncontrolledTooltip>
                  </td>
                  <td className="td-actions">
                    {request.status == 1 &&
                      <Button
                        className="btn-link"
                        color="danger"
                        id="tooltip653500053"
                        type="button"
                      >
                        <i className="tim-icons icon-simple-remove" />
                      </Button>
                    }
                    <UncontrolledTooltip
                      delay={0}
                      placement="left"
                      target="tooltip653500053"
                    >
                      Cancelar pedido
                    </UncontrolledTooltip>
                  </td>
                </tr>
              </tbody>
            )}
        </Table>
      }
    </div>
  )
}
