import React, { useState } from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, FormGroup, Label, Input, FormText } from 'reactstrap';
import { createRequest } from '../../store/action'
export default function FoodModal(props) {
  const { modal, toggle, values } = props
  const [client, setClient] = useState("")
  const [description, setDescription] = useState("")
  const [json, setJson] = useState(
    {
      "idProduct": values.id,
      "description": description,
      "client": client,
      "editor": sessionStorage.getItem('name')
    })
  return (
    <Modal style={{ backgroundImage: values.image }} isOpen={modal} modalClassName="modal-black" size="lg">
      <div className="modal-header">
        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={() => { toggle() }}>
          <i className="tim-icons icon-simple-remove"></i>
        </button>
        {/* <h5 className="modal-title">Pedido: {values.name}</h5> */}
      </div>
      <ModalBody>
        <form>
          <FormGroup>
            <Label for="exampleEmail">Nome do cliente</Label>
            <Input
              type="text"
              id="exampleEmail"
              placeholder="Nome do cliente"
              onChange={(e) => { setClient(e.target.value) }}
            />
          </FormGroup>
          <FormGroup>
            <Label for="examplePassword">Descrição</Label>
            <Input
              type="textarea"
              name="description"
              onChange={(e) => { setDescription(e.target.value) }}
              placeholder="Descrição"
              autoComplete="off"
            />
          </FormGroup>
        </form>
      </ModalBody>
      <ModalFooter>
        <Button
          block
          className="btn-simple"
          color="primary"
          href="#pablo"
          onClick={() => toggle()}
        >
          <i className="tim-icons icon-book-bookmark" /> CANCELAR
        </Button>
        <Button
          block
          className="btn-simple ml-1"
          color="success"
          href="#pablo"
          onClick={() => createRequest(json,toggle)}
        >
          <i className="tim-icons icon-bulb-63" /> CONFIRMAR
        </Button>
      </ModalFooter>
    </Modal>
  )
}
