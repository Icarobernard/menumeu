import React, { useState } from 'react';
import {
  Modal, Button, ModalHeader, ModalBody, Input, CardFooter, Card, InputGroup, InputGroupAddon, InputGroupText, Form, CardHeader, CardTitle, CardBody
} from "reactstrap";
import classnames from "classnames";
import { login } from "../../store/action"
function LoginModal(props) {
  const { toggle, modal } = props;
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  const [input, setInput] = useState('');

  const validateForm = () => {
    if (name == '' || password == '') {
      setInput('has-danger');
    } else {
      login({ name: name, password: password });
    }
  }
  return (
    <Modal
      isOpen={modal}
      toggle={toggle}
      modalClassName="modal-login"
    >
      <Card className="card-login" style={{ "border-radios":100 }}>
        <Form action="" className="form" method="">
          <CardHeader>
            <CardTitle tag="h4" className="text-info">Login</CardTitle>
            <button
              aria-label="Close"
              className="close"
              data-dismiss="modal"
              type="button"
              onClick={toggle}
            >
              <i className="tim-icons icon-simple-remove" />
            </button>
          </CardHeader>
          <CardBody>
            <InputGroup className={classnames(`input-lg ${input}`)}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="tim-icons icon-single-02" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                onChange={(e) => { setName(e.target.value) }}
                onFocus={() => { setInput('') }}
                placeholder="Nome..."
                type="text" />
            </InputGroup>
            <InputGroup className={classnames(`input-lg ${input}`)}>
              <InputGroupAddon addonType="prepend">
                <InputGroupText>
                  <i className="tim-icons icon-caps-small" />
                </InputGroupText>
              </InputGroupAddon>
              <Input
                onChange={(e) => { setPassword(e.target.value) }}
                onFocus={() => { setInput('') }}
                placeholder="Password..."
                type="password" />
            </InputGroup>
          </CardBody>
          <CardFooter className="text-center">
            <Button
              block
              className="btn-round"
              color="info"
              href="#pablo"
              onClick={e => validateForm()}
              size="lg"
            >
              Entrar
              </Button>
          </CardFooter>
        </Form>
      </Card>
    </Modal>
  );
}

export default LoginModal;