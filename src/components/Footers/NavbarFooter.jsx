/*!

=========================================================
* BLK Design System PRO React - v1.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/blk-design-system-pro-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import React from "react";
import { Link } from "react-router-dom";
// reactstrap components
import {

  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardTitle,
  Input,
  Button,
  Modal,
  Card,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Form,
  Navbar,
  Container,
  UncontrolledTooltip
} from "reactstrap";
import classnames from "classnames";

class ColorNavbar extends React.Component {
  state = {
    navbarColor: "navbar-transparent",
    modalLogin:false
  };

   toggleModalLogin =()=> {
    this.setState({
      modalLogin: !this.state.modalLogin
    });
  }
  componentDidMount() {
    window.addEventListener("scroll", this.changeNavbarColor);
  }
  componentWillUnmount() {
    window.removeEventListener("scroll", this.changeNavbarColor);
  }
  changeNavbarColor = () => {
    if (
      document.documentElement.scrollBottom > 299 ||
      document.body.scrollBottom > 299
    ) {
      this.setState({
        navbarColor: "bg-info"
      });
    } else if (
      document.documentElement.scrollBottom < 300 ||
      document.body.scrollBottom < 300
    ) {
      this.setState({
        navbarColor: "navbar-transparent"
      });
    }
  };
  render() {
    return (
      <>
        <Navbar className={"fixed-bottom " + this.state.navbarColor} expand="lg">
          <Container>
            <div className="navbar-translate">
              <Button
                className="btn-round"
                color="default"
                data-target="#loginModal"
                data-toggle="modal"
                onClick={()=>{this.toggleModalLogin()}}
              >
                <i className="nc-icon nc-lock-circle-open" />
          Login
        </Button>
            </div>
          </Container>
        </Navbar>
        <Button
          className="btn-round"
          color="default"
          data-target="#loginModal"
          data-toggle="modal"

        >
          <i className="nc-icon nc-lock-circle-open" />
          Login
        </Button>
        {/* Login Modal */}
        <Modal
          isOpen={this.state.modalLogin}
          toggle={this.toggleModalLogin}
          modalClassName="modal-login"
        >
          <Card className="card-login">
            <Form action="" className="form" method="">
              <CardHeader>
                <CardImg
                  alt="..."
                  src={require("assets/img/square-purple-1.png")}
                />
                <CardTitle tag="h4">Login</CardTitle>
                <button
                  aria-label="Close"
                  className="close"
                  data-dismiss="modal"
                  type="button"
                  onClick={this.toggleModalLogin}
                >
                  <i className="tim-icons icon-simple-remove" />
                </button>
              </CardHeader>
              <CardBody>
                <InputGroup className="input-lg">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="tim-icons icon-single-02" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="First Name..." type="text" />
                </InputGroup>
                <InputGroup className="input-lg">
                  <InputGroupAddon addonType="prepend">
                    <InputGroupText>
                      <i className="tim-icons icon-caps-small" />
                    </InputGroupText>
                  </InputGroupAddon>
                  <Input placeholder="Last Name..." type="text" />
                </InputGroup>
              </CardBody>
              <CardFooter className="text-center">
                <Button
                  block
                  className="btn-round"
                  color="primary"
                  href="#pablo"
                  onClick={e => e.preventDefault()}
                  size="lg"
                >
                  Get Started
        </Button>
              </CardFooter>
              <div className="pull-left ml-3 mb-3">
                <h6>
                  <a
                    className="link footer-link"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    Create Account
                   </a>
                </h6>
              </div>
              <div className="pull-right mr-3 mb-3">
                <h6>
                  <a
                    className="link footer-link"
                    href="#pablo"
                    onClick={e => e.preventDefault()}
                  >
                    Need Help?
                   </a>
                </h6>
              </div>
            </Form>
          </Card>
        </Modal>
      </>
    );
  }
}
const styles = {
  color: 'red',
  height: '50%'
}
export default ColorNavbar;
