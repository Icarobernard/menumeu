import React from "react";
// nodejs library that concatenates classes
import classnames from "classnames";
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardTitle,
  Form,
  Input,
  InputGroupAddon,
  InputGroupText,
  InputGroup,
  Container,
  Col
} from "reactstrap";
import { Link } from "react-router-dom";
// core components
import ColorNavbar from "components/Navbars/ColorNavbar.jsx";
import DemoFooter from "components/Footers/DemoFooter.jsx";
import { login, validateName } from "../../store/action"
import { validateLocaleAndSetLanguage } from "typescript";
class LoginPage extends React.Component {
  state = {
    name: "",
    password: "",
    changeInput:""
  };
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    document.body.classList.add("login-page");
 
  }
  componentWillUnmount() {
    document.body.classList.remove("login-page");
  }

  validate(){
    if(this.state.name == "" || this.state.password=="" ){
      this.setState({changeInput: "has-danger"})
    }else{
      login(this.state)
    }
    
  }
  render() {
    return (
      <>

        <div className="page-header">
          <div className="squares square1" />
          <div className="squares square2" />
          <div className="squares square3" />
          <div className="squares square4" />
          <div className="squares square5" />
          <div className="squares square6" />
          <div className="page-header-image" />
          <Container>
            <Col className="mx-auto" lg="5" md="8">
              <Card className="card-login">
                <Form action="" className="form" method="">
                  <CardHeader>
                    <CardImg
                      alt="..."
                      src={require("assets/img/square-purple-1.png")}
                    />
                    <CardTitle tag="h4">Login</CardTitle>
                  </CardHeader>
                  <CardBody>
                    <InputGroup
                      className={classnames(`input-lg ${this.state.changeInput}`, {
                        "input-group-focus": this.state.firstNameFocus
                      })}
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="tim-icons icon-single-02" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="username"
                        type="text"
                        onFocus={e => this.setState({ firstNameFocus: true, changeInput:"" })}
                        onBlur={e =>  console.log(this.state.name)}
                        onChange={e=>{this.setState({ name: e.target.value })} }
                      />
                    </InputGroup>
                    <InputGroup
                      className={classnames(`input-lg ${this.state.changeInput}`, {
                        "input-group-focus": this.state.lastNameFocus
                      })}
                    >
                      <InputGroupAddon addonType="prepend">
                        <InputGroupText>
                          <i className="tim-icons icon-caps-small" />
                        </InputGroupText>
                      </InputGroupAddon>
                      <Input
                        placeholder="password"
                        type="password"
                        onFocus={e => this.setState({ lastNameFocus: true, changeInput:"" })}
                        onBlur={e => this.setState({ lastNameFocus: false })}
                        onChange={e=>{this.setState({ password: e.target.value })}}
                      />
                    </InputGroup>
                  </CardBody>
                  <CardFooter className="text-center">
                    <Button
                      block
                      className="btn-round"
                      color="primary"
                      href="#pablo"
                      onClick={e => e.preventDefault()}
                      size="lg"
                      onClick={()=>{this.validate()}}
                    >
                      Get Started
                    </Button>
                  </CardFooter>
                  <div className="pull-left ml-3 mb-3">
                    <h6 className="link footer-link" >
                      <a
                        className="link footer-link"
                        href="#pablo"
                        onClick={() => { window.location.href = '/register-page' }}
                      >
                        Create account
                      </a>
                    </h6>
                  </div>
                  <div className="pull-right mr-3 mb-3">
                    <h6>
                      <a
                        className="link footer-link"
                        href="#pablo"
                        onClick={e => e.preventDefault()}
                      >
                        Need Help?
                      </a>
                    </h6>
                  </div>
                </Form>
              </Card>
            </Col>
          </Container>
        </div>
        <DemoFooter />
      </>
    );
  }
}

export default LoginPage;
