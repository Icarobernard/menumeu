
import React from "react";
// nodejs library that concatenates classes
// reactstrap components
import {
  Button,
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardImg,
  CardTitle,
  Form,
  Container,
  Col
} from "reactstrap";
// core components
import ColorNavbar from "components/Navbars/ColorNavbar.jsx";
import DemoFooter from "components/Footers/DemoFooter.jsx";
import NavbarFooter from "components/Footers/NavbarFooter"
import "assets/css/nucleo-icons.css";
class FeedPage extends React.Component {
  state = {
    items: [],
    erro: null
  };
  componentDidMount() {
    document.documentElement.scrollTop = 0;
    document.scrollingElement.scrollTop = 0;
    document.body.classList.add("login-page");
  }
  componentWillUnmount() {
    document.body.classList.remove("login-page");
  }
  render() {
    console.log(sessionStorage.getItem('values'))
    return (
      <>
          <div className="page-header">
          <div className="squares square1" />
          <div className="squares square2" />
          <div className="squares square3" />
          <div className="squares square4" />
          <div className="squares square5" />
          <div className="squares square6" />
          <div className="page-header-image" />
          <Container>
            <Col className="mx-auto" lg="5" md="8">
            </Col>
          </Container>
        </div>
        <NavbarFooter />
      </>
    );
  }
}

export default FeedPage;
